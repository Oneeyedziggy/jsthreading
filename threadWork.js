//TODO: accept a handler for what to do with each returned value? what to do at the end?
const threadWork = ( 
    workerCodeURI,
    arrayOfValues,
    threadCount = (navigator.hardwareConcurrency - 1) || 3 //default to available threads (-1 for main), or 3
  ) => {

  console.log( "threadCount: " + threadCount );
  return new Promise( (resolve, reject) => {
    try{
      if( typeof workerCodeURI !== "string" ){
        throw new TypeError( "workerCodeURI must be a string" ); //TODO: check for resource validity???
      } else if ( !Array.isArray( arrayOfValues ) ){
        throw new TypeError( "arrayOfValues must be an array" );
      } else if( typeof threadCount !== "number" || threadCount < 1 || Number.isNaN( threadCount ) ){
        throw new RangeError( "invalid thread count." );
      }
      console.time( "threaded" );
      let threadsAvailable = threadCount;
      let workers = {}

      //setup workers and kick each off once
      let workerObj, bufferVar;    
      for( let i = 0; i < threadCount; i++ ){
        workerObj = workers[ i ] = new Worker( workerCodeURI );
        //set a handler that will 
        workerObj.onmessage = ( message ) => {
          threadsAvailable++;
          //pickup a new task and run with it
          if( arrayOfValues.length > 0 ){
            threadsAvailable--;
            //grab the next item from arrayOfValues
            bufferVar = arrayOfValues.pop();
            //send the next item from arrayOfValues to the thread that just returned
            workers[ i ].postMessage( bufferVar );
          } else {
            if( threadsAvailable === threadCount ){
              console.timeEnd( "threaded" );
              resolve( "foo" );
            }
          }
        };
        if( arrayOfValues.length > 0 ){
          threadsAvailable--;
          bufferVar = arrayOfValues.pop();
          //console.log( "worker " + i + " processing " + bufferVar );
          workerObj.postMessage( bufferVar );
        }
      }
    } catch( err ){
      reject( err );
    }
  } );
};

//firefox is being picky about loading modules from script tags, especially from file://, fix this later
//export default (threadWork);