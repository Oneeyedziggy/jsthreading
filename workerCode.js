console.log( "Worker code loaded!" );
let isPrime = ( int ) => {
  let prime = true;
  for( let i = 2; i < int; i++){
    if( int % i === 0 ){
      prime = false;
      break;
    }
  }
  return prime;
}

onmessage = ( message ) => {
  console.log( "Worker code got message!" );
  //let numPrimes = 0;
  //for( let i = 0; i < message.data.length; i++ ){
  //  numPrimes += ( isPrime( message.data[ i ] ) + 0 );
  //}
  //postMessage( numPrimes );
  postMessage( isPrime( message.data ) );
}